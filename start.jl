
# plotting
using Plots
pyplot()

# Accents on the plot
using LaTeXStrings

# to parse jason files into structures
using JSON
