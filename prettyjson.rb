#!/usr/bin/env ruby

require 'json'

Dir.glob("./**/*.json").each do |path|
    txt = File.read(path)
    struct = JSON.parse(txt)
    File.write(path, JSON.pretty_generate(struct))
end
