include("skills.jl")
include("equipments.jl")



# Stats progression

function PartyStatsUpdate(ch::Character)
    statsGain = Stats(0.5, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1)*ch.level
    extraStats = Stats(0, 0, 0, 0, 0, 0, 0)
    for equip in ch.currentlyEquiped
        extraStats += equip.stats
    end
    if CharHasState(ch, stateCrack) != 0
        extraStats.atk += 40
        extraStats.agi += 40
    end
    ch.stats = statsGain*(ch.baseStats + extraStats)
end

# Stats

barbosa = Character("Barbosa", 100, 100, 100, 100, 100, 100, 100, PartyStatsUpdate)
bonJovi = Character("Bon Jovi", 100, 100, 100, 100, 100, 100, 100, PartyStatsUpdate)
cabide = Character("Cabide", 100, 130, 130, 70, 70, 100, 100, PartyStatsUpdate)
chanel = Character("Chanel", 100, 130, 100, 70, 70, 130, 100, PartyStatsUpdate)
carryOn = Character("Carry On", 100, 100, 100, 100, 100, 100, 100, PartyStatsUpdate)
denso = Character("Denso", 100, 100, 100, 100, 100, 100, 100, PartyStatsUpdate)
exemplo = Character("Exemplo", 100, 100, 100, 100, 100, 100, 100, PartyStatsUpdate)
japones = Character("Japonês", 100, 130, 100, 70, 70, 130, 100, PartyStatsUpdate)
magali = Character("Magali", 100, 100, 100, 100, 100, 100, 100, PartyStatsUpdate)
paulista = Character("Paulista", 100, 100, 100, 100, 100, 100, 100, PartyStatsUpdate)
pet = Character("Pet", 100, 100, 100, 100, 100, 100, 100, PartyStatsUpdate)
tsuhalo = Character("Tsuhalo", 100, 100, 100, 100, 100, 100, 100, PartyStatsUpdate)
zelda = Character("Zelda", 100, 100, 100, 100, 100, 100, 100, PartyStatsUpdate)


# Skills

cabide.skills = vcat(cabide.skills, (skillAtaque, 1))
cabide.skills = vcat(cabide.skills, (skillIpa, 1))
cabide.skills = vcat(cabide.skills, (skillRodinhaBarbara, 1))
cabide.skills = vcat(cabide.skills, (skillEmpurrar, 1))
cabide.skills = vcat(cabide.skills, (skillEspeto, 1))

chanel.skills = vcat(chanel.skills, (skillAtaque, 1))

japones.skills = vcat(japones.skills, (skillAtaque, 1))
japones.skills = vcat(japones.skills, (skillAtaqueNinja, 1))
japones.skills = vcat(japones.skills, (skillHolyWater, 11))
japones.skills = vcat(japones.skills, (skillCrack, 11))
japones.skills = vcat(japones.skills, (skillJogoDaVelha, 21))

# Equipments

japones.equips = vcat(japones.equips, espadas)
