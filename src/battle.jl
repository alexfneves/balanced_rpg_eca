include("character.jl")

mutable struct Battle
    party::Array{Character}
    enemies::Array{Character}
end

function BattleRun(b::Battle, turns::Integer)
    if turns == 0
        throw(ArgumentError("turns should > 0, and it's $(turns)"))
    end

    # Adjust variables at the beggining
    all = vcat(b.party, b.enemies)
    for ch in all
        for t = 1:turns
            push!(ch.turnData, TurnData(0, 0, 0, 0))
        end
        ch.turnData[1] = TurnData(ch.baseStats.mhp, 100, 0, 0)
        ch.states = Array{State}(0)
    end

    # Main loop
    for t = 1:turns
        println("Turn $t")

        # Order
        if size(all)[1] == 0
            order = []
        else
            order = [all[1]]
            if size(all)[1] > 1
                for c = 2:size(all)[1]
                    added = false
                    for o = 2:size(order)[1]
                        if all[c].stats.agi > order[o].stats.agi
                            insert!(order, o - 1, all[c])
                            added = true
                            break
                        end
                    end
                    if !added
                        push!(order, all[c])
                    end
                end
            end
        end

        # should throw exception if restrictions are not met, like using a skill but without enough TP, or using a skill that is not allowed (buff not present or not enough )
        for ch in order
            CharUpdateStates(ch)

            # A state could be added on the target, so his stats could have been changed
            for a in all
                a.statsUpdate(a)
            end

            # Check if skills to be used are ok
            if size(ch.actions)[1] < t
                continue
            end
            skillFound = false
            for skill in ch.skills
                if ch.actions[t] == skill[1]
                    skillFound = true
                    if skill[2] > ch.level
                        throw(ArgumentError("$(ch.actions[t]) in $(ch.name) skill list is for level $(skill[2])"))
                    end
                    break
                end
            end
            if !skillFound
                throw(ArgumentError("$(ch.actions[t]) not in $(ch.name) skill list"))
            end

            if ch in b.party
                targets = b.enemies
            else
                targets = b.party
            end

            # execute skill
            if ch.actions[t].tpCost > ch.turnData[t].tp
                throw(ArgumentError(" $(ch.name) can't use $(ch.actions[t]) (TP: $(ch.turnData[t].tp))"))
            else
                ch.turnData[t].tp += ch.actions[t].tpGain - ch.actions[t].tpCost
                if ch.turnData[t].tp < 0
                    ch.turnData[t].tp = 0
                elseif ch.turnData[t].tp >100
                    ch.turnData[t].tp = 100
                end
            end
            if ch.actions[t].target == "1 enemy"
                #select random target
                targets = [rand(targets)]
                println("$(ch.name) is attacking enemy $(targets[1].name) with $(ch.actions[t].name)")
            elseif ch.actions[t].target == "All enemies"
                println("$(ch.name) is attacking all enemies")
            elseif ch.actions[t].target == "Self"
                println("$(ch.name) is using $(ch.actions[t].name) on itself")
            else
                println("$(ch.name) is using $(ch.actions[t].name) on an unknown target")
            end
            for target in targets
                damage = round(Int, ch.actions[t].func(ch, target))
                target.turnData[t].hp -= damage
                ch.turnData[t].damageDone += damage
                println("$(ch.name) did $(damage) to $(target.name) (HP: $(target.turnData[t].hp))")
            end
        end

        # pass turnData to the next turn
        for ch in order
            if (t + 1) <= turns
                ch.turnData[t + 1] = deepcopy(ch.turnData[t])
            end
        end
    end
end

function BattlePlot(b::Battle, plotDamage = true, plotDPS = true, plotHP = true, plotMP = true, plotTP = true, plotParty = true, plotEnemies = false)
    if size([b.party; b.enemies])[1] <= 0
        throw(ArgumentError("no character to plot"))
    end
    allCharacters = []
    if plotParty
        allCharacters = vcat(allCharacters, b.party)
    end
    if plotEnemies
        allCharacters = vcat(allCharacters, b.enemies)
    end
    turns = size(allCharacters[1].turnData)[1]

    d = zeros(Integer, turns, size(allCharacters)[1])
    dps = zeros(Float64, turns, size(allCharacters)[1])
    hp = zeros(Integer, turns, size(allCharacters)[1])
    mp = zeros(Integer, turns, size(allCharacters)[1])
    tp = zeros(Integer, turns, size(allCharacters)[1])
    for t = 1:turns
        for i = 1:size(allCharacters)[1]
            d[t, i] = allCharacters[i].turnData[t].damageDone
            dps[t, i] = d[t, i]/t
            hp[t, i] = allCharacters[i].turnData[t].hp
            mp[t, i] = allCharacters[i].turnData[t].mp
            tp[t, i] = allCharacters[i].turnData[t].tp
        end
    end

    tit = ""
    lab = Array{LaTeXStrings.LaTeXString}(0)
    for p in b.party
        tit = "$tit $(p.name) $(p.level) "
        push!(lab, latexstring("$(p.name)"))
    end
    tit = "$tit\n VS \n"
    for e in b.enemies
        tit = "$tit $(e.name) $(e.level) "
        push!(lab, latexstring("$(e.name)"))
    end
    lab2 = Array{LaTeXStrings.LaTeXString, 2}(1, size(lab)[1])
    for i = 1:size(lab)[1]
        lab2[1, i] = lab[i]
    end
    plots = []
    i = 0
    if plotDamage
        push!(plots, plot(1:turns, d, title = "Damage", label = lab2))
        i += 1
    end
    if plotDPS
        push!(plots, plot(1:turns, dps, title = "DPS", label = lab2))
        i += 1
    end
    if plotHP
        push!(plots, plot(1:turns, hp, title = "HP", label = lab2))
        i += 1
    end
    if plotMP
        push!(plots, plot(1:turns, mp, title = "MP", label = lab2))
        i += 1
    end
    if plotTP
        push!(plots, plot(1:turns, tp, title = "TP", label = lab2))
        i += 1
    end
    allPlots = plot(plots..., layout = (i, 1), window_title = tit)
    display(allPlots)
end
