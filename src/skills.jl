include("states.jl")


AttackLevel = [1, 1.5, 2, 2.5]


# Generic

function SkillAtaque(a::Character, b::Character)
    ret = 4*a.stats.atk - 2*b.stats.def
    ret = ret > 0 ? ret : 0
    return ret
end
skillAtaque = Skill("Ataque", 0, 30, "1 enemy", SkillAtaque)

# Japonês

function SkillAtaqueNinja(a::Character, b::Character)
    CharAddState(a, stateTraveco)
    return AttackLevel[3]*SkillAtaque(a, b)
end
skillAtaqueNinja = Skill("Ataque Ninja", 40, 0, "1 enemy", SkillAtaqueNinja)

function SkillHolyWater(a::Character, b::Character)
    return AttackLevel[2]*2*a.stats.agi
end
skillHolyWater = Skill("Holy Water", 10, 60, "1 enemy", SkillHolyWater)

function SkillCrack(a::Character, b::Character)
    CharAddState(a, stateCrack)
    return 0
end
skillCrack = Skill("Crack", 30, 0, "Self", SkillCrack)

function SkillJogoDaVelha(a::Character, b::Character)
    return AttackLevel[4]*SkillAtaque(a, b)
end
skillJogoDaVelha = Skill("Crack", 30, 0, "Self", SkillJogoDaVelha)

# Cabide

function SkillRodinhaBarbara(a::Character, b::Character)
    return AttackLevel[1]*SkillAtaque(a, b)
end
skillRodinhaBarbara = Skill("Rodinha Bárbara", 40, 0, "All enemies", SkillRodinhaBarbara)

function SkillIpa(a::Character, b::Character)
    return 0
end
skillIpa = Skill("Rodinha Bárbara", 0, 30, "Self", SkillIpa)

function SkillEmpurrar(a::Character, b::Character)
    return AttackLevel[1]*SkillAtaque(a, b)
end
skillEmpurrar = Skill("Rodinha Bárbara", 40, 0, "1 enemy", SkillEmpurrar)

function SkillEspeto(a::Character, b::Character)
    return AttackLevel[1]*SkillAtaque(a, b)
end
skillEspeto = Skill("Rodinha Bárbara", 40, 0, "Self", SkillEspeto)
