include("character.jl")


# Stats progression

function EnemyStatsUpdate(ch::Character)
    # without the round, it won't run
    # this will be it as 200% at level 30 (it's 100% at level 1)
    statsGain = Stats(0.5, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1)*round(ch.level*(0.0344828*ch.level - 0.0344828))

    ch.stats = statsGain*ch.baseStats
end

# Parser Functions

function FindEnemy(name, enemies::Array{Character})
    for i = 1:size(enemies)[1]
        if (enemies[i].name == name)
            return i
        end
    end
    return 0
end


function ParseEnemies(fileName)
    dict = JSON.parsefile(fileName)
    enemies = Array{Character}(0)

    map(dict) do i
        try
            if i["name"] != ""
                enemy = Character(i["name"],
                        i["params"][1], i["params"][3], i["params"][4], i["params"][5], i["params"][6],
                        i["params"][7], i["params"][8], EnemyStatsUpdate)

                j = FindEnemy(i["name"], enemies)
                if j != 0
                    println("Warning: enemy $(i["name"]) already exists")
                end
                push!(enemies, enemy)
            end
        catch error
            println("empty enemy line")
        end
    end
    return enemies, dict
end


function PrintEnemies(fileName, enemies, dict)
    f = open(fileName, "w")
    dict2 = deepcopy(dict)

    map(dict2) do i
        try
            j = FindEnemy(i["name"], enemies)
            if j != 0
                i["params"][1] = enemies[j].baseStats.mhp
                i["params"][2] = 100 # starting mp is 100
                i["params"][3] = enemies[j].baseStats.atk
                i["params"][4] = enemies[j].baseStats.def
                i["params"][5] = enemies[j].baseStats.mat
                i["params"][6] = enemies[j].baseStats.mdf
                i["params"][7] = enemies[j].baseStats.agi
                i["params"][8] = enemies[j].baseStats.lck

                i["note"] = replace(i["note"], r"<Static Level(.*?)>", "<Static Level: $(enemies[j].level)>")
            end
        catch error
            println("error")
        end
    end

    JSON.print(f, dict2)
    close(f)
end
