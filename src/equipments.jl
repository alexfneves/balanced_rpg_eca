include("character.jl")

espada = Equipment("Espada", "Espada", Stats(0, 15, 0, 0, 0, 0, 0))
espadaDePrata = Equipment("Espada de Prata", "Espada", Stats(0, 25, 0, 0, 0, 0, 0))
espadas = [espada; espadaDePrata]

adaga = Equipment("Adaga", "Adaga", Stats(0, 10, 0, 0, 0, 0, 0))
adagas = [adaga]

cajado = Equipment("Cajado", "Cajado", Stats(0, 5, 0, 10, 0, 0, 0))
cajados = [cajado]




function FindEquipmentIndex(name, equipments::Array{Equipment})
    for i = 1:size(equipments)[1]
        if (equipments[i].name == name)
            return i
        end
    end
    return 0
end

function FindEquipment(name, equipments::Array{Equipment})
    return equipments[FindEquipmentIndex(name, equipments)]
end


function ParseEquipment(fileName, typeID)
    dict = JSON.parsefile(fileName)
    equipments = Array{Equipment}(0)

    map(dict) do i
        try
            if i["name"] != ""
                equipment = Equipment(i["name"], i[typeID], Stats(
                        i["params"][1], i["params"][3], i["params"][4], i["params"][5], i["params"][6],
                        i["params"][7], i["params"][8]))

                j = FindEquipmentIndex(i["name"], equipments)
                if j != 0
                    println("Warning: equipment $(i["name"]) already exists")
                end
                push!(equipments, equipment)
            end
        catch error
            println("empty equipment line")
        end
    end
    return equipments, dict
end

function PrintEquipment(fileName, equipments, dict)
    f = open(fileName, "w")
    dict2 = deepcopy(dict)

    map(dict2) do i
        try
            j = FindEquipmentIndex(i["name"], equipments)
            if j != 0
                while true
                    s = search(i["note"], r"<Percent(.*?)>")
                    if s != 0:-1
                        i["note"] = replace(i["note"], r"<Percent(.*?)>", "")
                    else
                        break
                    end

                    param = equipments[j].stats.atk
                    if param != 0
                        paramSign = param > 0 ? "+" : "" # the minus sign is atuomatic
                        i["note"] = i["note"]"<atk Plus: $(paramSign)$(param)>"
                    end
                    param = equipments[j].stats.def
                    if param != 0
                        paramSign = param > 0 ? "+" : "-"
                        i["note"] = i["note"]"<def Plus: $(paramSign)$(param)>"
                    end
                    param = equipments[j].stats.mat
                    if param != 0
                        paramSign = param > 0 ? "+" : "-"
                        i["note"] = i["note"]"<mat Plus: $(paramSign)$(param)>"
                    end
                    param = equipments[j].stats.mdf
                    if param != 0
                        paramSign = param > 0 ? "+" : "-"
                        i["note"] = i["note"]"<mdf Plus: $(paramSign)$(param)>"
                    end
                    param = equipments[j].stats.agi
                    if param != 0
                        paramSign = param > 0 ? "+" : "-"
                        i["note"] = i["note"]"<agi Plus: $(paramSign)$(param)>"
                    end
                    param = equipments[j].stats.lck
                    if param != 0
                        paramSign = param > 0 ? "+" : "-"
                        i["note"] = i["note"]"<lck Plus: $(paramSign)$(param)>"
                    end
                end
            end
        catch error
            println("error")
        end
    end

    JSON.print(f, dict2)
    close(f)
end
