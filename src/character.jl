struct State
    name
    minTurns::Int64
    maxTurns::Int64
end


mutable struct Skill
    name
    tpCost::Integer
    tpGain::Integer
    target
    func
end

mutable struct Stats
    mhp
    atk
    def
    mat
    mdf
    agi
    lck
end

import Base.+, Base.*

function +(s1::Stats, s2::Stats)
    s3 = Stats(s1.mhp + s2.mhp, s1.atk + s2.atk, s1.def + s2.def, s1.mat + s2.mat, s1.mdf + s2.mdf, s1.agi + s2.agi, s1.lck + s2.lck)
    return s3
end

function *(s1::Stats, s2::Stats)
    s3 = Stats(s1.mhp*s2.mhp, s1.atk*s2.atk, s1.def*s2.def, s1.mat*s2.mat, s1.mdf*s2.mdf, s1.agi*s2.agi, s1.lck*s2.lck)
    return s3
end

function *(s1::Stats, value)
    s3 = Stats(s1.mhp*value, s1.atk*value, s1.def*value, s1.mat*value, s1.mdf*value, s1.agi*value, s1.lck*value)
    return s3
end

struct Equipment
    name
    class
    stats::Stats
end

mutable struct TurnData
    hp::Integer
    mp::Integer
    tp::Integer
    damageDone::Integer
end

mutable struct StateTurn # connects a State to the remaining turns
    state::State
    turn::Integer
end

mutable struct Character
    name
    level::Integer
    baseStats::Stats # should never change
    stats::Stats
    statsUpdate
    turnData::Array{TurnData}
    states::Array{StateTurn}
    skills::Array{Tuple{Skill, Integer}}
    actions::Array{Skill}
    equips::Array{Equipment}
    currentlyEquiped::Array{Equipment}

    function Character(name, mhp, atk, def, mat, mdf, agi, lck, statsUpdate)
        baseStats = Stats(mhp, atk, def, mat, mdf, agi, lck)
        stats = Stats(0, 0, 0, 0, 0, 0, 0)
        new(name, 1, baseStats, stats, statsUpdate, Array{TurnData}(0), Array{StateTurn}(0), Array{Tuple{Skill, Integer}}(0), Array{Skill}(0), Array{Equipment}(0), Array{Equipment}(0))
    end
end


function CharHasState(char::Character, state::State)
    for i = 1:size(char.states)[1]
        if (char.states[i].state == state)
            return i
        end
    end
    return 0
end


function CharRemoveState(char::Character, state::State)
    p = CharHasState(char, state)
    if p != 0
        deleteat!(char.states, p)
    end
end


function CharAddState(char::Character, state::State)
    CharRemoveState(char, state)
    push!(char.states, StateTurn(state, rand(state.minTurns:state.maxTurns)))
end


function CharUpdateStates(char::Character)
    i = 1
    while true
        if i > size(char.states)[1]
            break
        end
        char.states[i].turn -= 1
        if char.states[i].turn < 1
            deleteat!(char.states, i)
        end
        i += 1
    end
end
