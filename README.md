

Instructions:


# Use once on the Julia console
Pkg.add("Plots")
Pkg.add("PyPlot")
Pkg.add("LaTeXStrings")
Pkg.add("JSON")


# Use this once when a Julia console is opened (starts PyPlot, JSON and LaTeXStrings)
include("start.jl")

# show a battle test
include("test.jl")



# Make the prettyjson.rb a executable, or execute it on the console with "ruby" (indent all json files)
ruby prettyjson.rb
