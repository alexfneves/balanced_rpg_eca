include("src/party.jl")
include("src/enemies.jl")
include("src/battle.jl")


enemies, dictEnemies = ParseEnemies("data/Enemies.json")
weapons, dictWeapons = ParseEquipment("data/Weapons.json", "wtypeId")
armors, dictArmors = ParseEquipment("data/Armors.json", "atypeId")


party = [japones; cabide; chanel]

japones.currentlyEquiped = vcat(japones.currentlyEquiped, espada)
chanel.currentlyEquiped = [adaga adaga]

japonesActions = Array{Any}(3)
japonesActions[1] = [skillAtaque; skillAtaque; skillAtaqueNinja; skillAtaque; skillAtaque; skillAtaque]
japonesActions[2] = [skillAtaque; skillHolyWater; skillCrack; skillHolyWater; skillAtaqueNinja; skillAtaqueNinja]
japonesActions[3] = [skillAtaque; skillHolyWater; skillCrack; skillHolyWater; skillAtaqueNinja; skillJogoDaVelha]
cabideActions = Array{Any}(3)
cabideActions[1] = [skillAtaque; skillAtaque; skillRodinhaBarbara; skillAtaque; skillAtaque; skillAtaque]
chanelActions = Array{Any}(3)
chanelActions[1] = [skillAtaque; skillAtaque; skillAtaque; skillAtaque; skillAtaque; skillAtaque]


for i = 1:3
    println("------- Battle $i")

    japones.actions = japonesActions[i]
    cabide.actions = cabideActions[1]
    chanel.actions = chanelActions[1]

    for ch in vcat(party, enemies[2:2])
        ch.level = 1 + 10*(i - 1)
        ch.turnData = Array{TurnData}(0)
        ch.states = Array{StateTurn}(0)
        ch.currentlyEquiped = Array{Equipment}(0)
    end

    b = Battle(party, enemies[2:2])
    BattleRun(b, 6)
    BattlePlot(b, true, true, false, false, true, true, false)
end
#=
PrintEnemies("data/Enemies2.json", enemies, dictEnemies)
PrintEquipment("data/Weapons2.json", weapons, dictWeapons)
PrintEquipment("data/Armors2.json", armors, dictArmors)
=#
